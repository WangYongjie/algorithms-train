package com.wenge.algorithms;

import java.util.Scanner;

public class A6 {


    static class Node{
        private int value;
        private Node next;

        private Node(int value){
            this.value = value;
        }
    }


    /**
     * 时间复杂度为O(n)
     * 设计思想：
     * 进行两次遍历：第一次确定第二次停止的时机
     * 第二次进行位置定位，只要找到倒数第K个节点的前一个节点
     * 然后绕过删除的节点，就可实现节点的删除。
     *
     *
     *
     *
     * @param args
     */


    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        int len = in.nextInt();

        int[] arr = new int[len];

        for(int i=0; i<len; i++){
            arr[i] = in.nextInt();
        }

        int lastKth = in.nextInt();

        in.close();

        Node head = new Node(len);

        Node cur = head;

        for(int i=0; i<len; i++){
            Node node = new Node(arr[i]);
            cur.next = node;
            cur = node;
        }

        head = head.next;

        Node result = removeLastKthNode(head, lastKth);

        while(result != null){
            System.out.print(result.value + " ");
            result = result.next;
        }

    }

    private static Node removeLastKthNode(Node head, int lastKth) {

        if(head == null || lastKth < 1){
            return head;
        }

        Node cur = head;

        while(cur != null){
            lastKth--;
            cur = cur.next;
        }

        if(lastKth == 0){
            head = head.next;
        }

        if(lastKth < 0){
            cur = head;
            while(++lastKth != 0){
                cur = cur.next;
            }

            cur.next = cur.next.next;
        }
        return head;

    }
}
