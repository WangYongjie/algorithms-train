package com.wenge.algorithms;

import java.util.Scanner;
import java.util.Stack;

public class A4 {


    /**
     * 用一个栈实现另一个的栈的排序（栈顶到栈底：从大到小）；
     * 设计思路：
     * 利用一个辅助栈，如果栈不为空，用该栈的出栈的元素和辅助栈的栈顶元素进行比较，如果大于
     * 辅助栈的栈顶元素，则将辅助栈都压到栈中，直到小于或者等于辅助栈的栈顶元素，才将这个元素压入辅助栈
     *
     *
     * @param args
     */


    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        int len = in.nextInt();

        int[] arr = new int[len];

        for(int i=0; i<len; i++){
            arr[i] = in.nextInt();
        }

        in.close();

        Stack<Integer> stack = new Stack<>();

        for(int i=0; i<len; i++){
            stack.push(arr[i]);
        }


        sortStackByStack(stack);


        while(!stack.isEmpty()){
            System.out.print(stack.pop() + " ");
        }


    }

    private static void sortStackByStack(Stack<Integer> stack) {
        Stack<Integer> help = new Stack<>();

        while(!stack.isEmpty()){
            int cur = stack.pop();

            while(!help.isEmpty() && cur > help.peek()){
                stack.push(help.pop());
            }
            help.push(cur);
        }

        while(!help.isEmpty()){
            stack.push(help.pop());
        }

    }
}
