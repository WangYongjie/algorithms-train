package com.wenge.algorithms;

import java.util.Scanner;
import java.util.Stack;

public class A3 {


    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        int len = in.nextInt();

        int[] arr = new int[len];

        for(int i=0; i<len; i++){
            arr[i] = in.nextInt();
        }

        in.close();

        Stack<Integer> stack = new Stack<>();

        for(int i=0; i<len; i++){
            stack.push(arr[i]);
        }

        reverse(stack);
        while(!stack.isEmpty()){
            System.out.print(stack.pop() + " ");
        }
    }
    private static void reverse(Stack<Integer> stack) {

        if(stack.isEmpty()){
            return;
        }
        int i = getAndRemoveLastElement(stack);
        reverse(stack);
        stack.push(i);
    }


    /**
     *
     * 取出栈的栈底元素的值：
     * 先出栈，如果栈不为空，就一直出栈，直到栈为空，
     * 栈为空的话，返回最后一个值，然后压栈，压的是最近一次弹出的值，也就是倒数第二个
     * 不断回归，反复执行else代码块，最后返回栈底元素
     *
     *
     *
     * @param stack
     * @return
     */

    private static int getAndRemoveLastElement(Stack<Integer> stack) {
        int result = stack.pop();
        if(stack.isEmpty()){
            return result;
        }else{
            int last = getAndRemoveLastElement(stack);
            stack.push(result);
            return last;
        }
    }

}
