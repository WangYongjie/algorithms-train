package com.wenge.algorithms;



import java.util.Scanner;
import java.util.Stack;

public class A1 {

    /**
     * 设计思想：
     * 首先用双栈解决问题：
     * 当前最小值一定存在最小栈的栈顶
     *
     * 而设计的这个栈的压栈：
     * 如果最开始，肯定第一个元素就是最小值，所以两个栈都要压栈
     * 然后进行判断，如果下一个进来的值比最小值小，那个最小值栈也要压栈
     * 不管什么情况，数值栈肯定要压栈
     *
     * 这个栈的出栈：
     * 数值栈肯定要出栈，而只有出栈的这个数值小于或者等于最小值的栈顶元素（也就是栈的最小值）
     * 才进行出栈,
     * 其实出栈的数值不会小于最小值栈的栈顶元素，只能大于或者等于，栈顶元素存着数值栈的最小值
     *
     *
     */
//    static class MyStack{
//        private Stack<Integer> stackData;
//        private Stack<Integer> stackMin;
//
//
//        public MyStack(){
//            this.stackData = new Stack<>();
//            this.stackMin = new Stack<>();
//        }
//
//
//        public void push(int newData){
//            if(this.stackMin.isEmpty()){
//                this.stackMin.push(newData);
//            }else if(newData <= this.getMin()){
//                this.stackMin.push(newData);
//            }
//
//            this.stackData.push(newData);
//        }
//
//
//        public int pop(){
//            if(this.stackData.isEmpty()){
//                throw new RuntimeException("Your Stack is Empty，You can't pop any numbers");
//            }
//
//            int value = this.stackData.pop();
//
//            if(value == this.getMin()){
//                this.stackMin.pop();
//            }
//
//            return value;
//        }
//
//        public int getMin(){
//            if(this.stackMin.isEmpty()){
//                throw new RuntimeException("Your Stack is Empty");
//            }
//            return this.stackMin.peek();
//        }
//    }
//
//    public static void main(String[] args) {
//        Scanner in = new Scanner(System.in);
//        int len = in.nextInt();
//
//        int[] arr = new int[len];
//
//        for(int i=0; i<len; i++){
//            arr[i] = in.nextInt();
//        }
//
//        in.close();
//
//        MyStack ms = new MyStack();
//
//        for(int i=0; i<len; i++){
//            ms.push(arr[i]);
//        }
//
//        ms.pop();
//
//        System.out.println(ms.getMin());
//
//
//
//    }
    static class MyStack{
        private Stack<Integer> stackData;
        private Stack<Integer> stackMin;

        public MyStack(){
            this.stackData = new Stack<>();
            this.stackMin = new Stack<>();
        }

        public void push(int newData){
            if(stackMin.isEmpty() || newData <= this.getMin()){
                this.stackMin.push(newData);
            }
            this.stackData.push(newData);
        }

        public int pop(){
            if(stackData.isEmpty()){
                throw new RuntimeException("This Stack is empty, you can't pop any Data");
            }
            int value = stackData.pop();
            if(value == stackMin.peek()){
                stackMin.pop();
            }
            return value;

        }

        public int getMin(){
            return stackMin.peek();
        }
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int len = in.nextInt();
        int[] arr = new int[len];
        for(int i = 0; i<len; i++){
            arr[i] = in.nextInt();
        }
        MyStack ms = new MyStack();

        for(int i=0; i<len; i++){
            ms.push(arr[i]);
        }

        System.out.println(ms.getMin());

        ms.pop();

        System.out.println(ms.getMin());
    }




}
