package com.wenge.algorithms;

import java.util.Scanner;

public class A8 {

    static class Node{
        int value;
        Node next;

        public Node(int value){
            this.value = value;
        }
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        int len = in.nextInt();

        int[] arr = new int[len];

        for(int i=0; i<len; i++){
            arr[i] = in.nextInt();
        }

        int m = in.nextInt();

        in.close();

        Node head = new Node(len);

        Node cur = head;

        for(int i=0; i<len; i++){
            Node node = new Node(arr[i]);
            cur.next = node;
            cur = node;
        }
        cur.next = head.next;

        head = head.next;

        Node res = josephusKill(head, m);

        System.out.println(res.value);


    }

    private static Node josephusKill(Node head, int m) {
        if(head == null || head.next == head || m < 1){
            return head;
        }

        Node last = head;
        while(last.next != head){
            last = last.next;
        }

        int count = 0;

        while(head != last){
            if(++count == m){
                last.next = head.next;
                count = 0;
            }else{
                last = last.next;
            }
            head = last.next;
        }
        return head;
    }
}
