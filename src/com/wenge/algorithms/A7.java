package com.wenge.algorithms;

import java.util.Scanner;
import java.util.Stack;

public class A7 {

    /**
     * 两个栈组成的队列：
     * 一个是压入栈，一个是弹出栈
     * 压入栈负责进队，弹出栈负责出队
     * 压入栈倒入弹出栈的条件：
     * 弹出栈必须为空，而且一次性要导入完
     *
     * 举例：
     * 进队：1   1 压入压入栈，之后倒入弹出栈，此时压入栈为空，弹出栈里面有1
     * 再次进队2， 此时由于弹出栈有1 不为空，没有办法倒进去。
     * 再次进队3， 也是由於彈出栈有1 不为空，没有办法倒进去，此时压入栈从栈顶到栈底依次为3、2
     * 再次进队4.  也是由於彈出栈有1 不为空，没有办法倒进去，此时压入栈从栈顶到栈底依次为4、3 、2
     * 最后出队1， 弹出栈为空，可以倒了，将4、3、2依次倒进弹出栈，此时弹出栈从栈顶到栈底依次为2、3、4,压入栈为空。
     *
     *
     *
     */



    static class TwoStacksQueue{
        private Stack<Integer> stackPush;
        private Stack<Integer> stackPop;

        private TwoStacksQueue(){
            stackPush = new Stack<>();
            stackPop = new Stack<>();
        }


        private void pushToPop(){
            if(stackPop.isEmpty()){
                while(!stackPush.empty()){
                    stackPop.push(stackPush.pop());
                }
            }
        }


        private void add(int pushInt){
            stackPush.push(pushInt);
            pushToPop();
        }

        private int poll(){
            if(stackPop.empty() && stackPush.empty()){
                throw new RuntimeException("Queue is empty");

            }

            pushToPop();
            return stackPop.pop();
        }

        private int peek(){
            if(stackPop.empty() && stackPush.empty()){
                throw new RuntimeException("Queue is empty");

            }
            pushToPop();
            return stackPop.peek();
        }
    }

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        int len = in.nextInt();

        int[] arr = new int[len];

        for(int i=0; i<len; i++){
            arr[i] = in.nextInt();
        }


        in.close();


        TwoStacksQueue twq = new TwoStacksQueue();

        for(int i=0; i<len; i++){
            twq.add(arr[i]);
        }

        System.out.println(twq.peek());


        while(! (twq.stackPush.empty() && twq.stackPop.empty())){
            System.out.print(twq.poll() + " ");
        }































    }
}
