package com.wenge.algorithms;

import java.util.Scanner;

public class A5 {

    static class Node{
        public int value;
        public Node next;

        public Node(int data){
            this.value = data;
        }
    }


    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        int len1 = in.nextInt();
        int[] arr1 = new int[len1];
        for (int i = 0; i < len1; i++) {
            arr1[i] = in.nextInt();
        }

        int len2 = in.nextInt();
        int[] arr2 = new int[len2];
        for(int i=0; i<len2; i++){
            arr2[i] = in.nextInt();
        }

        in.close();

        Node head1 = new Node(len1);
        Node cur = head1;
        for (int i = 0; i < len1; i++) {
            Node node = new Node(arr1[i]);
            cur.next = node;
            cur = node;
        }
        head1 = head1.next;

        Node head2 = new Node(len2);
        cur = head2;
        for (int i = 0; i < len2; i++) {
            Node node = new Node(arr2[i]);
            cur.next = node;
            cur = node;
        }
        head2 = head2.next;


        printCommonPart(head1, head2);

    }

    public static void printCommonPart(Node head1, Node head2) {

        while(head1 != null && head2 != null){
            if(head1.value < head2.value){
                head1 = head1.next;
            }else if (head1.value > head2.value){
                head2 = head2.next;
            }else{
                System.out.print(head1.value + " ");
                head1 = head1.next;
                head2 = head2.next;
            }
        }
        System.out.println();

    }

}
