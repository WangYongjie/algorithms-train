package com.wenge.algorithms;

import java.util.Scanner;

public class A10 {


    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        String str1 = in.next();
        String str2 = in.next();
        System.out.println(isDeformation(str1, str2));
        in.close();
    }

    private static boolean isDeformation(String str1, String str2) {
        if(str1 == null || str2 == null || str1.length() != str2.length()){
            return false;
        }

        char[] chas1 = str1.toCharArray();
        char[] chas2 = str2.toCharArray();

        int[] map = new int[256];

        for (char aChas1 : chas1) {
            map[aChas1]++;
        }

        for (char aChas2 : chas2) {
            if (map[aChas2]-- == 0) {
                //a-- 先用后减，一旦判断为0，说明已经没有存货了，再减一次，坑定为-1
                return false;
            }
        }
        return true;



    }
}
