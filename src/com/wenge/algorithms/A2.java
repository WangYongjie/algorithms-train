package com.wenge.algorithms;

import java.util.Scanner;

public class A2 {


    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        int rows = in.nextInt();
        int cols = in.nextInt();


        int[][] arr = new int[rows][cols];

        for(int i=0; i<rows; i++){
            for(int j=0; j<cols; j++){
                arr[i][j] = in.nextInt();
            }
        }


        int target = in.nextInt();

        in.close();

        boolean flag = solution(target, arr);

        System.out.println(flag);


    }

    private static boolean solution(int target, int[][] arr) {

        /**
         *
         * 设计思想：
         * 如果数组为递增数组，则从右上角开始比较
         * 目标数值大于选定位置，则往下走
         * 如果小于选定位置，则往左走
         * 二分法：
         *
         *
         */
        int rows = arr.length;
        int cols = arr[0].length;

        int row = 0;
        int col = cols - 1;
        while(row < rows && col >= 0){

            if(arr[row][col] < target){
                row ++;
            }else if(arr[row][col] > target){
                col --;
            }else{
                return true;
            }
        }
        return false;

    }
}
